# «Made With Creative Commons» translation project

This repository hosts the translation project for the
[Made With Creative Commons](https://madewith.cc/) book, by Paul
Stacey and Sarah Hinchliff Pearson.

The repository started off by pushing for Spanish translation only;
after it was
[accepted in the Weblate translation platform](https://hosted.weblate.org/projects/madewithcc/),
we decided to open it for any other translations. Welcome on board!

To contribute, please go to the
[project in Weblate](https://hosted.weblate.org/projects/madewithcc/translation/).

## Translation teams

- Translation into Spanish was started by
  [Leo Arias](https://elopio.net) and [Gunnar Wolf](https://gwolf.org);
  we welcome you on board if you can contribute with us.

  Please note that we are aiming to get the translated book printed by
  the [Economics Research Institute, UNAM](http://www.iiec.unam.mx/),
  so if you will contribute with a substantial translation (say, a
  page of printed text or more), we will appreciate if you can fill in
  (all fields highlighted in yellow), sign, scan and upload the
  [permission release to the University](./docs/coautores_unam.odt).

  Before starting to translate, please read the
  [norms for the Spanish translation](README_es.md).

- Translation into Norwegian Bookmål is coordinated by Petter
  Reinholdtsen, and conducted by a small team of dedicated people
  familiar with Weblate.  The primary translators are Ole-Erik Yrvin
  Ingrid Yrvin and Allan Nordhøy.

Any other teams, please add your information here.
